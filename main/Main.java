package main;
//Assuming I'm supposed to use an array and not a HashMap...
import java.util.Scanner;
import tinshoes.input.SafeScanner;
import tinshoes.geom.Circle;
import tinshoes.geom.Point;
public class Main {
	private static Circle.valType getValType(Scanner sc) {
		System.out.println("What value type would you like to use?");
		boolean isValid = true;
		while (true) {
			switch (sc.next().toLowerCase()) {
				case "r":
				case "rad":
				case "radius":
					return Circle.valType.RADIUS;
				case "d":
				case "diam":
				case "diameter":
					return Circle.valType.DIAMETER;
				case "c":
				case "cir":
				case "circ":
				case "circumfrence":
					return Circle.valType.CIRCUMFRENCE;
				case "a":
				case "area":
					return Circle.valType.AREA;
				default:
					System.err.println("That is not valid input.");
					break;
			}
		}
	}
	private static String errText = " is not valid input.";
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		//System.out.println("How many circles should be created?");
		int n;
		do {
			n = sc.nextInt("How many circles should be created?", " is not valid input.", true);
		} while (!(n > 0));
		Circle.valType t = getValType(usc);
		Circle[] circles = new Circle[n];
		for (int i = 0; i < n; i++) {
			System.out.println("Name this circle.");
			String name = usc.next();
			circles[i] = new Circle(sc.nextDouble("What is the value of \"" + name + "\"?", errText, true), t, new Point(sc.nextDouble("What is the x-value of the center of \"" + name + "\"?", " is not valid input.", true), sc.nextDouble("What is the y-value of the center of \"" + name + "\"?", " is not valid input.", true)), name);
		}
		System.out.println("What is the name of the circle you would like to modify? (This will only modify the first circle with this name.)");
		while (true) {
			String name = usc.next();
			t = getValType(usc);
			for (Circle c : circles) {
				if (c.getName().equals(name)) {
					System.out.println("Before: \n" + c);
					c.setAndCalc(sc.nextDouble("What is the value you would like to change the value type to?", " is not valid input.", true), t);
					if (sc.yn("Would you like to rename this circle?", "That is not valid input")) {
						System.out.println("What would you like to name it?");
						c.setName(usc.next());
					}
					System.out.println("After: \n" + c);
					return;
				}
			}
			System.out.println("No circle with the name \"" + name + "\" was found.  Please enter a valid name.");
		}
	}
}
